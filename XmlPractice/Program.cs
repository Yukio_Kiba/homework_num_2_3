﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace XmlPractice
{
    class Program
    {
        static void ParseXMLToConsole(string path)
        {
            var document = XDocument.Load(path);
            if (document != null && document.Root.HasElements)
            {
                var query = document.Element("Companies").Elements("Company").Elements("Department").GroupBy(x => x.Parent);

                foreach (var item in query)
                {
                    Console.WriteLine(item.Key.Attribute("name").Value + " : ");
                    foreach (var inner in item)
                    {
                        Console.WriteLine("\t" + inner.Attribute("adress").Value + " from " + inner.Attribute("dateOfOpening").Value);
                    }
                }
            }
        }

        static void ChangeXMLDocument(string path, string elementByName, string attribute, string genAttributeValue, string finValue)
        {
            var doc = XDocument.Load(path);
            
            if(doc != null && doc.Root.HasElements)
            {
                XElement rootElem = doc.Element("Companies");

                if(rootElem != null)
                {
                    var query = rootElem.Elements(elementByName).Where(x => x.Attribute(attribute).Value == genAttributeValue);
                    foreach (var item in query)
                    {
                        item.Attribute(attribute).Value = finValue;
                    }
                }
            }
            doc.Save(path);
            
        }

        static void CreateXMLDocument(string path)
        {
            XElement rootElem = new XElement("Companies");

            XElement compElem = new XElement("Company");
            compElem.SetAttributeValue("name", "Vans");
            compElem.SetAttributeValue("dateOfCreation", "01.01.1998");

            XElement deptEllem1 = new XElement("Department");
            deptEllem1.SetAttributeValue("location", "Zel str Aspen");
            deptEllem1.SetAttributeValue("dateOfOpening", "30.10.2001");

            XElement deptEllem2 = new XElement("Department");
            deptEllem2.SetAttributeValue("location", "Lmao str Aspen");
            deptEllem2.SetAttributeValue("dateOfOpening", "05.11.2001");

            compElem.Add(deptEllem1);
            compElem.Add(deptEllem2);

            rootElem.Add(compElem);

            XDocument newDoc = new XDocument(rootElem);
            newDoc.Save(path);
        }
        static void Main(string[] args)
        {
            string path = @"D:\ELEKS\Sharp_HomeWork\GenericPractice\GenericPractice\Company.xml";
            ParseXMLToConsole(path);
            ChangeXMLDocument(path, "Company", "name", "Zara", "Zara Inc");
            CreateXMLDocument(@"CustomXML.xml");
            Console.ReadKey();
        }
    }
}
