﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericPractice
{
    class Program
    {
        #region LinqSort
        static IEnumerable<T> GetSortedCustomList<T>(MyShellForList<T> other)
            where T: Human
        {
            var query = from item in other
                        where item.Age > 10
                        orderby item.Age descending
                        select item;
            return query;
        }
        #endregion

        static void Main(string[] args)
        {
            //MyShellForList<int> my = new MyShellForList<int>() { 6, 21, 6, 8, 99 };
            //MyShellForList<ShellForPrimitives<int>> my = new MyShellForList<ShellForPrimitives<int>>();
            MyShellForList<Human> my = new MyShellForList<Human>() { new Human(5, "Andriy"), new Human(8, "Jora"),
                new Human(2, "Lev"), new Human(15, "Lel"), new Human(20, "Ogo"), new Human(12, "Vitaliy"), new Human(32, "Oleg") };
            my.MySort(0, my.Count - 1);
            #region LinqPractice
            var haha = GetSortedCustomList(my);

            var query = (from item in haha
                         where item.Name == "Ogo"
                         select item).FirstOrDefault();

            foreach (var item in haha)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(query);
            #endregion
            Console.ReadKey();
        }
    }

    class Human : IComparable
    {
        public int Age { get; set; }
        public string Name{ get; set; }
        public Human(int age, string name)
        {
            Age = age;
            this.Name = name;
        }

        public int CompareTo(object obj)
        {
            if (obj is Human)
                return this.Age.CompareTo((obj as Human).Age);
            else
                throw new InvalidOperationException();
        }
        public override string ToString()
        {
            return this.Age + " - " + this.Name;
        }
    }
    //class ShellForPrimitives<T> : ICloneable, IComparable<T>
    //    where T : IComparable<T>
    //{
    //    T _field;
    //    public T TheField
    //    {
    //        get { return _field; }
    //        set { _field = value; }
    //    }

    //    public ShellForPrimitives()
    //    {
    //        _field = default(T);
    //    }
    //    public ShellForPrimitives(T field)
    //    {
    //        this._field = field;
    //    }
    //    public object Clone()
    //    {
    //        return new ShellForPrimitives<T>(this.TheField);
    //    }

    //    public int CompareTo(T other)
    //    {
    //        return this._field.CompareTo(other);
    //    }
    //}

    static class CutomExtension
    {
        //public static void MyLinqSort(this IList<Human> other)
        //{
        //    var query = from item in other
        //                orderby item.Age
        //                select item;

        //}
        public static void MySort<T>(this IList<T> other, int l, int r)
            where T : IComparable
        {
            T temp;
            T x = other[l + (r - l) / 2];

            int i = l;
            int j = r;

            while (i <= j)
            {
                while (other[i].CompareTo(x) < 0) i++;
                while (other[j].CompareTo(x) > 0) j--;
                if (i <= j)
                {
                    temp = other[i];
                    other[i] = other[j];
                    other[j] = temp;
                    i++;
                    j--;
                }
            }
            if (i < r)
                MySort(other, i, r);

            if (l < j)
                MySort(other, l, j);
        }
    }

    class MyShellForList<T> : IEnumerable<T>, IList<T>
        where T : IComparable
    {
        private List<T> _mainContainer;

        public MyShellForList()
        {
            _mainContainer = new List<T>();
        }
        public MyShellForList(MyShellForList<T> some)
        {
            _mainContainer = some._mainContainer;
        }
        public IEnumerator<T> GetEnumerator()
        {
            if (_mainContainer.Count == 0)
                throw new InvalidOperationException();
            foreach (T item in _mainContainer)
            {
                yield return item;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (_mainContainer.Count == 0)
                throw new InvalidOperationException();
            foreach (var item in _mainContainer)
            {
                yield return item;
            }
        }

        public object Clone()
        {
            return new MyShellForList<T>(this);
        }

        public int CompareTo(object obj)
        {
            if (obj is ICollection<T>)
                return this._mainContainer.Count.CompareTo((obj as ICollection<T>).Count);

            throw new InvalidOperationException();
        }

        public T this[int index]
        {
            get
            {
                if (index < _mainContainer.Count && index >= 0)
                    return _mainContainer[index];
                else
                    return default(T);
            }
            set { if(index < _mainContainer.Count && index >= 0) _mainContainer[index] = value; }
        }

        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public void Add(T item)
        {
            _mainContainer.Add(item);
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return _mainContainer.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

    }
}
